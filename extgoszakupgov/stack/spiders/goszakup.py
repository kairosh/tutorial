# -*- encoding: utf-8 -*-
__author__ = 'kkairat'


from scrapy.contrib.spiders import CrawlSpider, Rule

import logging
from datetime import datetime
from scrapy import Spider, Request
from stack.items import  HtmlPage
import scrapy
import time
#from BeautifulSoup import BeautifulSoup


class OrphanSpider(CrawlSpider):
    items = []
    name = "extgoszakup"
    allowed_domains = ["portal.goszakup.gov.kz"]
    start_urls = ["http://portal.goszakup.gov.kz/portal/index.php/ru/publictrade/showlot/16198307"]


    def parse2(self, response):
        yield scrapy.FormRequest.from_response(response,
            formname='log4in_form_head',
            formdata={'user_login': 'kairo', 'user_password': 'karo'},
            callback=self.parse_item)

    def parse(self, response):
         yield Request(response.url, callback = self.parse3)
         urls=response.xpath('.//*[@id="trade"]/table//a/@href').extract()
         for url in urls:
             site=response.url[0:response.url.find(response.url.split('/')[-1])]+url
             logging.info('extract links----------- %s', site)
             time.sleep(1)
             yield Request(site, callback = self.parse3)


    def parse3(self, response):
            #open_in_browser(response)
            logging.info('extract parse Tender Page------------- %s', response.url)
#----------------------------------------------------------------------------------------------------
        # --   Обработка позиций лотов
        #for i in range(2,30,1):
            item = HtmlPage()
            item['urlTenderPage'] = response.url
            item['domain'] = 'portal.goszakup.gov.kz'
            item['cdate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            item['udate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            #----------------------------------
            item['client_ru'] = response.xpath('.//*[@id="trade"]/div[2]//tr[4]/td[2]/text()')[0].extract()
            item['open_date'] = response.xpath('.//*[@id="trade"]/div[2]//tr[7]/td[2]/text()')[0].extract()
            item['close_date'] = response.xpath('.//*[@id="trade"]/div[2]//tr[8]/td[2]/text()')[0].extract()

            #try:
            j=1
            sqlvalue='.//*[@id="trade"]/div[2]/table[2]//tr[%d]/td[2]/text()'%j
            item['name_ru'] = response.xpath(sqlvalue)[0].extract()
            item['len_name_ru'] = len(response.xpath(sqlvalue)[0].extract())
            logging.info(response.xpath(sqlvalue)[0].extract())
            logging.info('-------------------------')
            j=2
            sqlvalue='.//*[@id="trade"]/div[2]/table[2]//tr[%d]/td[2]/text()'%j
            desc=response.xpath(sqlvalue)[0].extract()
            j=3
            sqlvalue='.//*[@id="trade"]/div[2]/table[2]//tr[%d]/td[2]/text()'%j
            desc=desc+' '+response.xpath(sqlvalue)[0].extract()
            item['description_ru'] = desc
            item['len_desc_ru'] = len(desc)
            j=4
            sqlvalue='.//*[@id="trade"]/div[2]/table[2]//tr[%d]/td[2]/text()'%j
            item['qty'] =response.xpath(sqlvalue)[0].extract()
            j=5
            sqlvalue='.//*[@id="trade"]/div[2]/table[2]//tr[%d]/td[2]/text()'%j
            item['measure'] =response.xpath(sqlvalue)[0].extract()
            j=6
            sqlvalue='.//*[@id="trade"]/div[2]/table[2]//tr[%d]/td[2]/text()'%j
            item['price'] =response.xpath(sqlvalue)[0].extract()
            j=9
            sqlvalue='.//*[@id="trade"]/div[2]/table[2]//tr[%d]/td[2]/text()'%j
            item['delivery'] =response.xpath(sqlvalue)[0].extract()
            j=10
            sqlvalue='.//*[@id="trade"]/div[2]/table[2]//tr[%d]/td[2]/text()'%j
            item['place'] =response.xpath(sqlvalue)[0].extract()
            self.items.append(item)
            #except:
            #    ok=1

            return  item



