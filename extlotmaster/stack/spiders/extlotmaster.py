# -*- encoding: utf-8 -*-
__author__ = 'kkairat'


from scrapy.contrib.spiders import CrawlSpider, Rule

import logging
from datetime import datetime

from stack.items import  HtmlPage
import scrapy
#from BeautifulSoup import BeautifulSoup


class OrphanSpider(CrawlSpider):
    name = "extlotmaster"
    allowed_domains = ["kz.lotmaster.kz"]
    start_urls = ["http://kz.lotmaster.kz/tender.php?lot=8222620001112"]


    def parse2(self, response):
        yield scrapy.FormRequest.from_response(response,
            formname='log4in_form_head',
            formdata={'user_login': 'kairo', 'user_password': 'karo'},
            callback=self.parse_item)

    def parse(self, response):
        #open_in_browser(response)
        logging.info('extract parse Tender Page------------- %s', response.url)
        item = HtmlPage()

        item['urlTenderPage'] = response.url
        item['domain'] = 'kz.lotmaster.kz'
        item['cdate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['udate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['name_ru'] = response.xpath('.//td[@class="dettab2"]/b/text()')[0].extract()
        item['len_name_ru'] = len(response.xpath('.//td[@class="dettab2"]/b/text()')[0].extract())
        item['description_ru'] = response.xpath('.//td[@class="dettab2"]/text()')[1].extract()+' '+response.xpath('.//td[@class="dettab2"]/text()')[2].extract()
        item['len_desc_ru'] = len(response.xpath('.//td[@class="dettab2"]/text()')[1].extract()+' '+response.xpath('.//td[@class="dettab2"]/text()')[2].extract())
        item['place'] =response.xpath('.//td[@class="dettab2"]/b/text()')[1].extract()
        item['price'] =response.xpath('.//td[@class="dettab2"]/b/text()')[2].extract()
        item['open_date'] =response.xpath('.//td[@class="dettab2"]/text()')[9].extract()
        item['close_date'] =response.xpath('.//td[@class="dettab2"]/text()')[10].extract()
#----------------------------------------------------------------------------------------
        logging.info('--OK!!!!!!!!!!!-----------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/text()')[0].extract())
        logging.info('-------------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/b/text()')[0].extract())
        logging.info('-------------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/text()')[2].extract())
        logging.info('-------------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/text()')[3].extract())
        logging.info('-------------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/text()')[4].extract())
        logging.info('-------------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/text()')[5].extract())
        logging.info('-------------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/text()')[6].extract())
        logging.info('-------------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/text()')[7].extract())
        logging.info('-------------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/text()')[8].extract())
        logging.info('-------------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/text()')[9].extract())
        logging.info('-------------------------')
        logging.info(response.xpath('.//td[@class="dettab2"]/text()')[10].extract())
        logging.info('-------------------------')

        return  item

