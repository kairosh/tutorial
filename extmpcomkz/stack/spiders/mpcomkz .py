# -*- encoding: utf-8 -*-
__author__ = 'kkairat'


from scrapy.contrib.spiders import CrawlSpider, Rule

import logging
from datetime import datetime
from scrapy import Spider, Request
from stack.items import  HtmlPage
import scrapy
import time
from selenium import webdriver
#from BeautifulSoup import BeautifulSoup
#browser = webdriver.Firefox()

class OrphanSpider(CrawlSpider):
    items = []
    name = "extmpcomkz"
    allowed_domains = ["mp.com.kz"]
    #start_urls = ["https://mp.com.kz/frontend/Login.action"]
    start_urls = [u"https://mp.com.kz/tender/21286-Саморез"]


    def get_cookies(self):
        driver = webdriver.Firefox()
        driver.implicitly_wait(30)
        base_url = "https://mp.com.kz/frontend/Login.action"
        driver.get(base_url)
        driver.find_element_by_name("loginEmail").clear()
        driver.find_element_by_name("loginEmail").send_keys("kairo@list.ru")
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys("kai12345")
        driver.find_element_by_name("login").click()
        cookies = driver.get_cookies()

        driver.close()
        return cookies

    def parse(self, response):
        self.cookies = self.get_cookies()
        yield Request(response.url, cookies = self.cookies, callback = self.parse_item)

    def parse_item(self, response):

            logging.info('extract parse Tender Page------------- %s', response.url)
#----------------------------------------------------------------------------------------------------
        # --   Обработка позиций лотов
        #for i in range(2,30,1):
            item = HtmlPage()
            item['urlTenderPage'] = response.url
            item['domain'] = 'mp.com.kz'
            item['cdate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            item['udate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            #----------------------------------
            j=2
            sqlvalue='.//*[@id="auction_div"]//tr[%d]/td[2]/text()'%j
            item['name_ru'] = response.xpath(sqlvalue)[0].extract().strip()
            item['len_name_ru'] = len(response.xpath(sqlvalue)[0].extract().strip())
            logging.info(response.xpath(sqlvalue)[0].extract().strip())
            logging.info('-------------------------')
            j=7
            sqlvalue='.//*[@id="auction_div"]//tr[%d]/td[2]/strong/a/text()'%j
            item['client_ru'] =response.xpath(sqlvalue)[0].extract().strip()

            j=9
            sqlvalue='.//*[@id="auction_div"]//tr[%d]/td[2]/strong/text()'%j
            item['price'] =response.xpath(sqlvalue)[0].extract().strip()

            j=10
            sqlvalue='.//*[@id="auction_div"]//tr[%d]/td[2]/strong/text()'%j
            item['qty'] =response.xpath(sqlvalue)[0].extract().strip()
            j=14
            sqlvalue='.//*[@id="auction_div"]//tr[%d]/td[2]/pre/text()'%j
            txt=response.xpath(sqlvalue)[0].extract().strip()
            pz=txt.find(u'Место поставки товара:')
            desc=txt[0:pz].strip()
            item['description_ru'] =desc
            item['len_desc_ru'] = len(desc)
            j=15
            sqlvalue='.//*[@id="auction_div"]//tr[%d]/td[2]/pre/text()'%j
            item['delivery'] =response.xpath(sqlvalue)[0].extract().strip()
            j=17
            sqlvalue='.//*[@id="auction_div"]//tr[%d]/td[2]/pre/text()'%j
            item['place'] =response.xpath(sqlvalue)[0].extract().strip()

            self.items.append(item)

            return  item

