# -*- encoding: utf-8 -*-
__author__ = 'kkairat'


from scrapy.contrib.spiders import CrawlSpider, Rule

import logging
from datetime import datetime
from scrapy import Spider, Request
from stack.items import  HtmlPage
import scrapy
import time
#from BeautifulSoup import BeautifulSoup


class OrphanSpider(CrawlSpider):
    items = []
    name = "exttenderkz"
    allowed_domains = ["tender.kz"]
    start_urls = ["https://tender.kz/Tenders/Bid-id-5193714"]


    def parse2(self, response):
        yield scrapy.FormRequest.from_response(response,
            formname='log4in_form_head',
            formdata={'user_login': 'kairo', 'user_password': 'karo'},
            callback=self.parse_item)

    def parse(self, response):
         yield Request(response.url, callback = self.parse3)
         #urls=response.xpath('.//*[@id="trade"]/table//a/@href').extract()
         #for url in urls:
         #    site=response.url[0:response.url.find(response.url.split('/')[-1])]+url
         #    logging.info('extract links----------- %s', site)
         #    time.sleep(1)
         #    yield Request(site, callback = self.parse3)


    def parse3(self, response):
            #open_in_browser(response)
            logging.info('extract parse Tender Page------------- %s', response.url)
#----------------------------------------------------------------------------------------------------
        # --   Обработка позиций лотов
        #for i in range(2,30,1):
            item = HtmlPage()
            item['urlTenderPage'] = response.url
            item['domain'] = 'tender.kz'
            item['cdate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            item['udate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            #----------------------------------
            #item['client_ru'] = response.xpath('.//*[@id="trade"]/div[2]//tr[4]/td[2]/text()')[0].extract()
            #try:
            #j=1
            sqlvalue='.//strong[@class="lot-title"]/text()'
            item['name_ru'] = response.xpath(sqlvalue)[0].extract().strip()
            item['len_name_ru'] = len(response.xpath(sqlvalue)[0].extract().strip())
            logging.info(response.xpath(sqlvalue)[0].extract().strip())
            logging.info('-------------------------')
            j=3
            sqlvalue='.//ul[@class="lot-list"]/li[%d]/strong/text()'%j
            desc=response.xpath(sqlvalue)[0].extract().strip()
            logging.info(desc)
            item['description_ru'] = desc
            item['len_desc_ru'] = len(desc)
            j=6
            sqlvalue='.//ul[@class="lot-list"]/li[%d]/span/text()'%j
            txt =response.xpath(sqlvalue)[0].extract().strip()
            if txt in u'Единица измерения':
                sqlvalue='.//ul[@class="lot-list"]/li[%d]/strong/text()'%j
                item['measure'] =response.xpath(sqlvalue)[0].extract().strip()
                logging.info(response.xpath(sqlvalue)[0].extract().strip())
            j=7
            sqlvalue='.//ul[@class="lot-list"]/li[%d]/span/text()'%j
            txt =response.xpath(sqlvalue)[0].extract().strip()
            if txt in u'Цена за единицу, тенге':
                sqlvalue='.//ul[@class="lot-list"]/li[%d]/strong/text()'%j
                item['price'] =response.xpath(sqlvalue)[0].extract().strip()
                logging.info(response.xpath(sqlvalue)[0].extract().strip())
            j=8
            sqlvalue='.//ul[@class="lot-list"]/li[%d]/span/text()'%j
            txt =response.xpath(sqlvalue)[0].extract().strip()
            if txt in u'Количество':
                sqlvalue='.//ul[@class="lot-list"]/li[%d]/strong/text()'%j
                item['qty'] =response.xpath(sqlvalue)[0].extract().strip()
                logging.info(response.xpath(sqlvalue)[0].extract().strip())
#----------------------------------------------
            j=5
            sqlvalue='.//ul[@class="lot-list"][2]/li[%d]/strong/text()'%j
            txt  =response.xpath(sqlvalue)[0].extract().strip()
            sqlvalue='.//ul[@class="lot-list"][2]/li[%d]/strong/em/text()'%j
            txt+=' '+response.xpath(sqlvalue)[0].extract().strip()
            logging.info(txt)
            item['address_ru'] =txt

            j=9
            sqlvalue='.//ul[@class="lot-list"][2]/li[%d]/strong/text()'%j
            item['open_date'] =response.xpath(sqlvalue)[0].extract().strip()
            logging.info(response.xpath(sqlvalue)[0].extract().strip())
            j=10
            sqlvalue='.//ul[@class="lot-list"][2]/li[%d]/strong/text()'%j
            item['close_date'] =response.xpath(sqlvalue)[0].extract().strip()

            self.items.append(item)
            #except:
            #    ok=1

            return  item
