# -*- encoding: utf-8 -*-
__author__ = 'kkairat'


from scrapy.contrib.spiders import CrawlSpider, Rule

import logging
from datetime import datetime
from scrapy import Spider, Request
from stack.items import  HtmlPage
import scrapy
import time
from selenium import webdriver
#from BeautifulSoup import BeautifulSoup
#browser = webdriver.Firefox()

class OrphanSpider(CrawlSpider):
    items = []
    name = "exttenderplus"
    allowed_domains = ["tenderplus.kz"]
    start_urls = ["https://tenderplus.kz/lot/show/9437793"]


    def get_cookies(self):
        driver = webdriver.Firefox()
        driver.implicitly_wait(30)
        base_url = "https://mp.com.kz/frontend/Login.action"
        driver.get(base_url)
        driver.find_element_by_name("loginEmail").clear()
        driver.find_element_by_name("loginEmail").send_keys("kairo@list.ru")
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys("kai12345")
        driver.find_element_by_name("login").click()
        cookies = driver.get_cookies()

        driver.close()
        return cookies

    def parse(self, response):
       # self.cookies = self.get_cookies()
        #yield Request(response.url, cookies = self.cookies, callback = self.parse_item)
        yield Request(response.url,  callback = self.parse_item)

    def parse_item(self, response):
            logging.info('extract parse Tender Page------------- %s', response.url)
        # --   Обработка позиций лотов
        #for i in range(2,30,1):
            item = HtmlPage()
            item['urlTenderPage'] = response.url
            item['domain'] = 'tenderplus.kz'
            item['cdate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            item['udate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            #----------------------------------
            j=2
            sqlvalue='.//*[@id="unreg_title_text"]/p/text()'
            item['name_ru'] = response.xpath(sqlvalue)[0].extract().strip()
            item['len_name_ru'] = len(response.xpath(sqlvalue)[0].extract().strip())
            logging.info(response.xpath(sqlvalue)[0].extract().strip())
            logging.info('-------------------------')
            j=9
            sqlvalue='.//*[@id="one_cost"]/text()'
            item['price'] =response.xpath(sqlvalue)[0].extract().strip()
            j=10
            sqlvalue='.//*[@id="count"]/text()'
            item['qty'] =response.xpath(sqlvalue)[0].extract().strip()

            sqlvalue='.//*[@id="one_lot_t"]/tbody/tr/td[2]/span/text()'
            item['measure'] =response.xpath(sqlvalue)[0].extract().strip()

            j=6
            sqlvalue='.//*[@id="main_t"]/tbody/tr[%d]/td[1]/text()'%j
            place=response.xpath(sqlvalue)[0].extract().strip()
            if place in u'Место поставки':
                sqlvalue='.//*[@id="main_t"]/tbody/tr[%d]/td[2]/text()'%j
                item['place'] =response.xpath(sqlvalue)[0].extract().strip()
                try:
                    j=8
                    sqlvalue='.//*[@id="main_t"]/tbody/tr[%d]/td[2]/div/text()'%j
                    item['description_ru'] =response.xpath(sqlvalue)[0].extract().strip()
                    item['len_desc_ru'] =len(response.xpath(sqlvalue)[0].extract().strip())
                except:
                    desc='no'
                j=5
                sqlvalue='.//*[@id="main_t"]/tbody/tr[%d]/td[2]/text()'%j
                item['client_ru'] =response.xpath(sqlvalue)[0].extract().strip()
            else:
                j=7
                sqlvalue='.//*[@id="main_t"]/tbody/tr[%d]/td[2]/text()'%j
                item['place'] =response.xpath(sqlvalue)[0].extract().strip()
                try:
                    j=9
                    sqlvalue='.//*[@id="main_t"]/tbody/tr[%d]/td[2]/div/text()'%j
                    item['description_ru'] =response.xpath(sqlvalue)[0].extract().strip()
                    item['len_desc_ru'] =len(response.xpath(sqlvalue)[0].extract().strip())
                except:
                    desc='no'
                j=6
                sqlvalue='.//*[@id="main_t"]/tbody/tr[%d]/td[2]/text()'%j
                item['client_ru'] =response.xpath(sqlvalue)[0].extract().strip()


            self.items.append(item)

            return  item
