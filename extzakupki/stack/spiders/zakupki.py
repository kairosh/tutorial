# -*- encoding: utf-8 -*-
__author__ = 'kkairat'


from scrapy.contrib.spiders import CrawlSpider, Rule

import logging
from datetime import datetime

from stack.items import  HtmlPage
import scrapy
#from BeautifulSoup import BeautifulSoup


class OrphanSpider(CrawlSpider):
    name = "extzakupki"
    allowed_domains = ["zakupki.kz"]
    start_urls = ["http://zakupki.kz/tender/22398946/"]


    def parse2(self, response):
        yield scrapy.FormRequest.from_response(response,
            formname='log4in_form_head',
            formdata={'user_login': 'kairo', 'user_password': 'karo'},
            callback=self.parse_item)

    def parse(self, response):
        #open_in_browser(response)
        logging.info('extract parse Tender Page------------- %s', response.url)
        item = HtmlPage()
        logging.info('--OK!!!!!!!!!!!-----------------------')
        item['urlTenderPage'] = response.url
        item['domain'] = 'zakupki.kz'
        item['cdate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['udate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['name_ru'] = response.xpath('.//div[@class="table-title"]/text()')[0].extract()
        item['len_name_ru'] = len(response.xpath('.//div[@class="table-title"]/text()')[0].extract())
        item['description_ru'] = response.xpath('.//div[@class="col-md-6"]//tr[2]/td/blockquote/p/text()')[0].extract()
        item['len_desc_ru'] = len(response.xpath('.//div[@class="col-md-6"]//tr[2]/td/blockquote/p/text()')[0].extract())

        for i in range(5,13,1):
            sqlname='.//div[@class="col-md-6"]//tr[%d]/td[1]/text()'%i
            sqlvalue='.//div[@class="col-md-6"]//tr[%d]/td[2]/text()'%i
            try:
                txt=response.xpath(sqlname)[0].extract()
                txt=' '.join(txt.split())
                if txt==u'Единица измерения':
                    item['measure'] =response.xpath(sqlvalue)[0].extract()
                    logging.info('Ed IZM  --- '+response.xpath(sqlvalue)[0].extract())
                    logging.info('-------------------------')
                if txt==u'Цена за единицу, тенге':
                    item['price'] =response.xpath(sqlvalue)[0].extract()
                if txt==u'Количество':
                    item['qty'] =response.xpath(sqlvalue)[0].extract()
                if txt==u'Место поставки товара':
                    item['place'] =response.xpath(sqlvalue)[0].extract()
                if txt==u'Условия поставки':
                    item['delivery'] =response.xpath(sqlvalue)[0].extract()
                if txt==u'Вид деятельности':
                        adrkato=response.xpath(sqlvalue).extract()
                        p=''
                        for ad in adrkato:
                            p+=ad
                            p+=' '
                        item['kodKATO'] =p

                if txt==u'Дата и время начала предоставления конкурсных заявок':
                    item['open_date'] =response.xpath(sqlvalue)[0].extract()
                if txt==u'Дата и время окончания предоставления конкурсных заявок':
                    item['close_date'] =response.xpath(sqlvalue)[0].extract()
                if txt==u'Место поставки товара(КАТО)':
                    adrkato=response.xpath(sqlvalue).extract()
                    p=''
                    for ad in adrkato:
                        p+=ad
                        p+=' '
                    item['delivery_kodKATO'] =p
            except:
                continue
            logging.info('.'+txt+'.')
            logging.info('-------------------------')

        txt=response.xpath('.//*[@id="page"]/div[8]/div[2]//tr[5]/td[2]/text()')[0].extract()
        txt=' '.join(txt.split())
        if txt==u'Местонахождение заказчика':
            item['address_ru'] =response.xpath('.//*[@id="page"]/div[8]/div[2]//tr[5]/td[2]/text()')[0].extract()


        return  item


