# -*- encoding: utf-8 -*-
__author__ = 'kkairat'

from scrapy.contrib.spiders import CrawlSpider, Rule

import logging
from datetime import datetime
from scrapy import Spider, Request
from stack.items import  HtmlPage
import scrapy
import time
#from BeautifulSoup import BeautifulSoup


class OrphanSpider(CrawlSpider):
    items = []
    name = "exttenderia"
    allowed_domains = ["tenderiya.kz"]
    start_urls = ["http://www.tenderiya.kz/tenders/details/?lotid=1632437"]


    def parse2(self, response):
        yield scrapy.FormRequest.from_response(response,
            formname='log4in_form_head',
            formdata={'user_login': 'kairo', 'user_password': 'karo'},
            callback=self.parse_item)

    def parse(self, response):
         yield Request(response.url, callback = self.parse3)
         urls=response.xpath('.//*[@id="tenders"]//a/@href').extract()
         for url in urls:
             if url in '#':
                 continue
             site=response.url[0:response.url.find(response.url.split('/')[-3])]+url
             logging.info('extract links----------- %s', site)
             time.sleep(1)
             yield Request(site, callback = self.parse3)


    def parse3(self, response):
            #open_in_browser(response)
            logging.info('extract parse Tender Page------------- %s', response.url)
#----------------------------------------------------------------------------------------------------
        # --   Обработка позиций лотов
        #for i in range(2,30,1):
            item = HtmlPage()
            item['urlTenderPage'] = response.url
            item['domain'] = 'tenderiya.kz'
            item['cdate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            item['udate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            #----------------------------------
            item['client_ru'] = response.xpath('.//*[@class="tender-info"]/div[2]/p/text()')[10].extract()
            item['open_date'] = response.xpath('.//*[@class="tender-info"]/div[2]/p/text()')[15].extract()
            item['close_date'] = response.xpath('.//*[@class="tender-info"]/div[2]/p/text()')[15].extract()

            #try:
            j=3
            sqlvalue='.//*[@class="tender-info"]/div/p[%d]/text()'%j
            item['name_ru'] = response.xpath(sqlvalue)[3].extract().strip()
            item['len_name_ru'] = len(response.xpath(sqlvalue)[3].extract().strip())
            logging.info(response.xpath(sqlvalue)[3].extract())
            logging.info('-------------------------')
            j=4
            sqlvalue='.//*[@class="tender-info"]/div/p[%d]/text()'%j
            item['place'] =response.xpath(sqlvalue)[7].extract().strip()
            j=5
            sqlvalue='.//*[@class="tender-info"]/div/p[%d]/text()'%j
            item['price'] =response.xpath(sqlvalue)[3].extract().strip()
            j=6
            sqlvalue='.//*[@class="tender-info"]/div/p[%d]/text()'%j
            item['qty'] =response.xpath(sqlvalue)[1].extract().strip()
            j=7
            sqlvalue='.//*[@class="tender-info"]/div/p[%d]/text()'%j
            item['measure'] =response.xpath(sqlvalue)[1].extract().strip()

            j=10
            sqlvalue='.//*[@class="tender-info"]/div/p[%d]/text()'%j
            item['delivery'] =response.xpath(sqlvalue)[1].extract().strip()

            self.items.append(item)


            return  item
