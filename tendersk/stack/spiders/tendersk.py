# -*- encoding: utf-8 -*-
__author__ = 'kkairat'


from scrapy.contrib.spiders import CrawlSpider, Rule

import logging
from datetime import datetime

from stack.items import  HtmlPage
import scrapy
#from BeautifulSoup import BeautifulSoup


class OrphanSpider(CrawlSpider):
    name = "exttendersk"
    allowed_domains = ["tender.sk.kz"]
    start_urls = ["http://tender.sk.kz/index.php/ru/negs/show/205858/3"]


    def parse2(self, response):
        yield scrapy.FormRequest.from_response(response,
            formname='log4in_form_head',
            formdata={'user_login': 'kairo', 'user_password': 'karo'},
            callback=self.parse_item)

    def parse(self, response):
        #open_in_browser(response)
        logging.info('extract parse Tender Page------------- %s', response.url)
        items = []

        logging.info('--OK!!!!!!!!!!!-----------------------')

#----------------------------------------------------------------------------------------------------
        # --   Обработка позиций лотов
        for i in range(2,30,1):
            item = HtmlPage()
            item['urlTenderPage'] = response.url
            item['domain'] = 'tender.sk.kz'
            item['cdate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            item['udate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            #----------------------------------
            item['client_ru'] = response.xpath('.//*[@id="container"]//tr[3]/td[2]/text()')[0].extract()
            item['open_date'] = response.xpath('.//*[@id="container"]//tr[7]/td[2]/text()')[0].extract()
            item['close_date'] = response.xpath('.//*[@id="container"]//tr[8]/td[2]/text()')[0].extract()
            try:
                j=2
                sqlvalue='.//*[@id="container"]//div[2]//tr[%d]/td[%d]/text()'%(i,j)
                item['name_ru'] = response.xpath(sqlvalue)[0].extract()
                item['len_name_ru'] = len(response.xpath(sqlvalue)[0].extract())
                logging.info(response.xpath(sqlvalue)[0].extract())
                logging.info('-------------------------')
                j=3
                sqlvalue='.//*[@id="container"]//div[2]//tr[%d]/td[%d]/text()'%(i,j)
                item['description_ru'] = response.xpath(sqlvalue)[0].extract()
                item['len_desc_ru'] = len(response.xpath(sqlvalue)[0].extract())
                j=4
                sqlvalue='.//*[@id="container"]//div[2]//tr[%d]/td[%d]/text()'%(i,j)
                item['qty'] =response.xpath(sqlvalue)[0].extract()
                j=5
                sqlvalue='.//*[@id="container"]//div[2]//tr[%d]/td[%d]/text()'%(i,j)
                item['price'] =response.xpath(sqlvalue)[0].extract()
                j=7
                sqlvalue='.//*[@id="container"]//div[2]//tr[%d]/td[%d]/text()'%(i,j)
                item['place'] =response.xpath(sqlvalue)[0].extract()
                j=8
                sqlvalue='.//*[@id="container"]//div[2]//tr[%d]/td[%d]/text()'%(i,j)
                item['delivery'] =response.xpath(sqlvalue)[0].extract()
                items.append(item)

            except:
                break

        return  items

